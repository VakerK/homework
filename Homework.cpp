﻿#include <iostream>
#include <string>

class Animal
{
public:
	virtual void Sound() const = 0;
};

class Cat : public Animal
{
public:
	void Sound() const override
	{
		std::cout << "Meow\n";
	}
};

class Dog : public Animal
{
public:
	void Sound() const override
	{
		std::cout << "Woof\n";
	}
};

class Cow : public Animal
{
public:
	void Sound() const override
	{
		std::cout << "Moo\n";
	}
};

class Leo : public Animal
{
public:
	void Sound() const override
	{
		std::cout << "Gao\n";
	}
};

int main()
{
	Animal* animals[4];
	animals[0] = new Cat();
	animals[1] = new Dog();
	animals[2] = new Cow();
	animals[3] = new Leo();

	for (Animal* a : animals)
		a->Sound();
}